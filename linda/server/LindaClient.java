package linda.server;

/** Client part of a client/server implementation of Linda.
 * It implements the Linda interface and propagates everything to the server it is connected to.
 * */
public class LindaClient extends LindaClientGeneric<LindaServer> {
	
	public LindaClient(String serverURI) {
		super(serverURI);
	}
}
