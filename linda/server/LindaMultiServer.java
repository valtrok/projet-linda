package linda.server;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collection;

import linda.CallbackWrapper;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.RemoteCallbackInterface;
import linda.Tuple;
import linda.shm.CentralizedLinda;

public class LindaMultiServer extends UnicastRemoteObject implements LindaMultiServerInterface, Serializable {
	
	private CentralizedLinda linda;
	private ArrayList<LindaMultiServerClient> clients;
	private LindaMultiServerInterface master;
	private boolean isMaster;
	private int port;
	
	private static String MASTER_URL = "rmi://172.22.226.180:1099/master";
	private static int MASTER_PORT = 1099;
	private static int START_PORT = 10000;
	
	public LindaMultiServer() throws RemoteException {
		linda = new CentralizedLinda();
		this.isMaster = false;
		this.port = START_PORT;
		this.clients = new ArrayList<>();
		try {
			this.master = this.getMaster();
			if (!this.isMaster) {
				int port = this.master.getNewPort();
				ArrayList<String> servers = this.master.getClients();
				for (String server : servers) {
					this.addClient(server);
				}
				
				this.createRegistry(port);
				
				String address;
				try {
					address = "rmi://" + Inet4Address.getLocalHost().getHostAddress() + ":" + port + "/linda";
				} catch (UnknownHostException e) {
					address = "rmi://localhost:" + port + "/linda";
				}
				
				try {
					java.rmi.Naming.rebind(address, this);
					this.master.addClient(address);
				} catch (RemoteException e) {
					e.printStackTrace();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}		
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	private void createRegistry(int port) {
		try {
            LocateRegistry.createRegistry(port);
        } catch (java.rmi.server.ExportException e) {
        } catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	private LindaMultiServerInterface getMaster() throws MalformedURLException, RemoteException {
		LindaMultiServerInterface server = null;
		try {
			// Try to find the master
			server = (LindaMultiServerInterface) Naming.lookup(MASTER_URL);
			this.isMaster = false;
		} catch (MalformedURLException | RemoteException | NotBoundException exc) {
			// If no master found, create it
			this.createRegistry(MASTER_PORT);
			java.rmi.Naming.rebind(MASTER_URL, this);
			this.isMaster = true;
			return this;
		}
		return server;
	}

	@Override
	public void addClient(String address) throws RemoteException {
		LindaMultiServerClient client = new LindaMultiServerClient(address);
		if (this.isMaster) {
			for (LindaMultiServerClient c : this.clients) {
				c.addClient(client.getURI());
			}
		}
		this.clients.add(client);
	}

	@Override
	public int getNewPort() throws RemoteException {
		return this.port++;
	}
	
	@Override
	public ArrayList<String> getClients() throws RemoteException {
		ArrayList<String> servers = new ArrayList<>();
		for (LindaMultiServerClient client : this.clients) {
			servers.add(client.getURI());
		}
		return servers;
	}

	@Override
	public synchronized void wakeUp() throws RemoteException {
		this.notifyAll();
	}
	
	@Override
	public void write(Tuple t) throws RemoteException {
		linda.write(t);
		for (LindaMultiServerClient client : this.clients) {
			client.wakeUp();
		}
	}

	@Override
	public synchronized Tuple take(Tuple template) throws RemoteException {
		Tuple t;
		t = this.tryTakeEverywhere(template);
		if (t != null) {
			return t;
		}
		do {
			try {
				this.wait();
				t = this.tryTakeEverywhere(template);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while(t == null);
		return t;
	}

	@Override
	public synchronized Tuple read(Tuple template) throws RemoteException {
		Tuple t;
		t = this.tryReadEverywhere(template);
		if (t != null) {
			return t;
		}
		do {
			try {
				this.wait();
				t = this.tryReadEverywhere(template);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while(t == null);
		return t;
	}

	private Tuple tryTakeEverywhere(Tuple template) throws RemoteException {
		Tuple t = this.tryTake(template);
		if (t != null) {
			return t;
		}
		t = this.master.tryTake(template);
		if (this.clients.size() > 0 && t == null) {
			int i = 0;
			do {
				t = this.clients.get(i).tryTake(template);
				i++;
			} while(i<this.clients.size() && t == null);
		}
		return t;
	}
	
	private Tuple tryReadEverywhere(Tuple template) throws RemoteException {
		Tuple t = this.tryRead(template);
		if (t != null) {
			return t;
		}
		int i = 0;
		t = this.master.tryRead(template);
		if (this.clients.size() > 0 && t == null) {
			do {
				t = this.clients.get(i).tryRead(template);
				i++;
			} while(i<this.clients.size() && t == null);
		}
		return t;
	}
	
	@Override
	public Tuple tryTake(Tuple template) throws RemoteException {
		return linda.tryTake(template);
	}

	@Override
	public Tuple tryRead(Tuple template) throws RemoteException {
		return linda.tryRead(template);
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) throws RemoteException {
		return linda.takeAll(template);
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) throws RemoteException {
		return linda.readAll(template);
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, RemoteCallbackInterface callback) throws RemoteException {
		linda.eventRegister(mode, timing, template, new CallbackWrapper(callback));
	}
	
	@Override
	public void debug(String prefix) throws RemoteException {
		System.out.println(prefix + "debug called");
	}
}
