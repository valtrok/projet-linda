package linda;

import java.rmi.RemoteException;
import java.io.Serializable;
import java.rmi.Remote;

public interface RemoteCallbackInterface extends Remote, Serializable {

    public void call(final Tuple t) throws RemoteException;
}
