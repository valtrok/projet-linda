package linda.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;

import linda.CallbackWrapper;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.RemoteCallbackInterface;
import linda.Tuple;
import linda.shm.ConcurrentCentralizedLinda;

public class LindaMonoServer extends UnicastRemoteObject implements LindaServer {
	
	private ConcurrentCentralizedLinda linda;

	public LindaMonoServer() throws RemoteException {
		linda = new ConcurrentCentralizedLinda();
	}
	
	@Override
	public void write(Tuple t) throws RemoteException {
		linda.write(t);
	}

	@Override
	public Tuple take(Tuple template) throws RemoteException {
		return linda.take(template);
	}

	@Override
	public Tuple read(Tuple template) throws RemoteException {
		return linda.read(template);
	}

	@Override
	public Tuple tryTake(Tuple template) throws RemoteException {
		return linda.tryTake(template);
	}

	@Override
	public Tuple tryRead(Tuple template) throws RemoteException {
		return linda.tryRead(template);
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) throws RemoteException {
		return linda.takeAll(template);
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) throws RemoteException {
		return linda.readAll(template);
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, RemoteCallbackInterface callback) throws RemoteException {
		linda.eventRegister(mode, timing, template, new CallbackWrapper(callback));
	}
	
	@Override
	public void debug(String prefix) throws RemoteException {
		System.out.println(prefix + "debug called");
	}
}
