package linda.test;

import java.lang.management.*;

import linda.*;
import linda.shm.ConcurrentCentralizedLinda;

public class PerformanceTest {

	
    public static void main(String[] a) {
        
        final Linda linda = new ConcurrentCentralizedLinda();
        // final Linda linda = new linda.server.LindaClient("//localhost:4000/aaa");
                
        new Thread() {
            public void run() {
            	ThreadMXBean tmxb = ManagementFactory.getThreadMXBean();
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motifis = new Tuple(Integer.class, String.class);
                Tuple motifii = new Tuple(Integer.class, Integer.class);
                for(int i = 0; i < 100; i++) {
	                Tuple t4 = linda.read(motifis);	                
	                Tuple t5 = linda.take(motifii);	
	                Tuple t6 = linda.take(motifii);
                }
                long cputime = tmxb.getCurrentThreadCpuTime();
                System.out.println("Thread 1, time in cpu : " + cputime / 1000.0 / 1000.0);
            }
        }.start();
                
        new Thread() {
        	
            public void run() {
            	ThreadMXBean tmxb = ManagementFactory.getThreadMXBean();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for(int i = 0; i < 100; i++) {
	                Tuple t1 = new Tuple(4, 5);
	                linda.write(t1);
	                Tuple t11 = new Tuple(4, 5);
	                linda.write(t11);
	                Tuple t2 = new Tuple("hello", 15);
	                linda.write(t2);
	                Tuple t3 = new Tuple(4, "foo");
	                linda.write(t3);
                }
                long cputime = tmxb.getCurrentThreadCpuTime();
                System.out.println("Thread 2, time in cpu : " + cputime / 1000.0 / 1000.0);
            }
        }.start();
    }
}
