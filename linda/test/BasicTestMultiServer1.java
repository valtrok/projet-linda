package linda.test;

import linda.*;

public class BasicTestMultiServer1 {

    public static void main(String[] a) {
    	
    	final Linda linda2000 = new linda.server.LindaClient("rmi://localhost:10000/linda");
        final Linda linda2001 = new linda.server.LindaClient("rmi://localhost:10001/linda");
        
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Tuple motif = new Tuple(Integer.class, String.class);
                Tuple res = linda2001.take(motif);
                System.out.println("(1) Resultat:" + res);
                linda2001.debug("(1)");
            }
        }.start();
                
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Tuple t1 = new Tuple(4, 5);
                System.out.println("(2) write: " + t1);
                linda2000.write(t1);

                Tuple t11 = new Tuple(4, 5);
                System.out.println("(2) write: " + t11);
                linda2000.write(t11);

                Tuple t2 = new Tuple("hello", 15);
                System.out.println("(2) write: " + t2);
                linda2000.write(t2);

                Tuple t3 = new Tuple(4, "foo");
                System.out.println("(2) write: " + t3);
                linda2000.write(t3);
                                
                linda2000.debug("(2)");

            }
        }.start();
                
    }
}
